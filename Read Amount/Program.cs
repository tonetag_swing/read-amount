﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Read_Amount
{
    class Program
    {
        static void Main(string[] args)
        {
            string inPath = args[0];

            Console.WriteLine("Preprocessing image...");
            preProcessingImage(inPath);

            Console.WriteLine("Converting to text...");
            convertToText(inPath);

            Console.WriteLine("Parsing tsv file and finding total amount...");
            string tsvFile = "outTest.tsv";
            List<Record> records = ReadAllRecodes(tsvFile);

            Double amount = findAmountBasedOnHeight(records);
            Console.WriteLine("TOTAL:" + amount);
        }

        public static void preProcessingImage(string inPath)
        {
            //string command = "convert\\convert.exe " + inPath + " -type Grayscale -resize 200% -units PixelsPerInch -density 300 -threshold 60% " + inPath;
            string command = "convert\\convert.exe " + inPath + " -type Grayscale -resize 200% -units PixelsPerInch -density 300 " + inPath;
            runCommand(command);
        }

        public static void convertToText(String imageFilePath)
        {
            //string command = "Tesseract-OCR\\tesseract.exe " + imageFilePath + " outTest --psm 11 tsv";
            string command = "Tesseract-OCR\\tesseract.exe " + imageFilePath + " outTest --psm 11 -c tessedit_create_tsv=1 -l Devanagari -c tessedit_char_whitelist=\"0123456789 -₹$,.:%/\"";
            runCommand(command);
        }

        private static List<Record> ReadAllRecodes(string tsvFile)
        {
            String[] content = File.ReadAllLines(tsvFile);
            var outList = new List<Record>();
            foreach (string line in content)
            {
                string[] records = line.Split('\t');
                if (records.Length == 12)
                {
                    Record record = new Record();
                    record.setLeft(records[6]);
                    record.setTop(records[7]);
                    record.setWidth(records[8]);
                    record.setHeight(records[9]);
                    record.setConf(records[10]);
                    record.setText(records[11]);
                    outList.Add(record);
                }
            }
            return outList;
        }

        private static Double findAmountBasedOnHeight(List<Record> records)
        {
            int heightHighValue = 0;
            Double amount = 0.00;
            Console.WriteLine("height\tconf\ttext");
            foreach (Record record in records) // Loop through List with foreach
            {
                string height = record.getHeight();
                string text = record.getText();
                string conf = record.getConf();
                int confidence = 0;
                int currentTextHeight = 0;
                try
                {
                    currentTextHeight = int.Parse(height);
                    confidence = int.Parse(conf);
                }
                catch (Exception e) { }


                if (!string.IsNullOrEmpty(text) && !text.Equals("-1"))
                {
                    string s = text.Substring(0, 1);
                    try { int i = int.Parse(s); }
                    catch (Exception e) { text = text.Substring(1, text.Length - 1); }

                    string[] numbers = Regex.Split(text, @"(\D)\s * ([.\d,] +)");
                    foreach (string value in numbers)
                    {
                        if (!string.IsNullOrEmpty(value))
                        {
                            try
                            {
                                Double amntd = Double.Parse(value);
                                string sam = amntd + "";
                                if (sam.Contains('.'))
                                {
                                    sam = sam.Substring(0, sam.IndexOf('.'));
                                }
                                if (sam.Length <= 5)
                                {
                                    Console.WriteLine(height + "\t" + conf + "\t" + text);
                                    if (confidence >= 80)
                                    {
                                        if (currentTextHeight >= heightHighValue)
                                        {
                                            //if (amntd > amount) {
                                            Console.WriteLine("Updating amount: " + amntd);
                                            amount = amntd;
                                            heightHighValue = currentTextHeight;
                                            //}
                                        }
                                    }
                                }
                            }
                            catch (Exception e) { }
                        }
                    }
                }

            }
            return amount;
        }
        public static string runCommand(string command)
        {
            string res = "";
            try
            {
                //Console.WriteLine("Running command: " + command);
                ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + command);
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;

                // wrap IDisposable into using (in order to release hProcess) 
                Process process = new Process();
                process.StartInfo = procStartInfo;
                process.Start();
                // and only then read the result
                res = process.StandardOutput.ReadToEnd();
                // Add this: wait until process does its work
                process.WaitForExit();
                //Console.WriteLine("result: {0}", res);
            }
            catch (Exception e) { }
            return res;
        }

        private class Record
        {
            //level:page_num:block_num: par_num: line_num: word_num: left: top: width: height: conf:text:

            private string top;//7
            private string left;//6
            private string conf;//10
            private string text;//11
            private string width;//8
            private string level;//0
            private string height;//9
            private string par_num;//3
            private string line_num;//4
            private string word_num;//5
            private string page_num;//1
            private string block_num;//2

            public void setTop(string top)
            {
                this.top = top;
            }
            public string getTop()
            {
                return top;
            }
            public void setLeft(string left)
            {
                this.left = left;
            }
            public string getLeft()
            {
                return left;
            }
            public void setHeight(string height)
            {
                this.height = height;
            }
            public string getHeight()
            {
                return height;
            }
            public void setConf(string conf)
            {
                this.conf = conf;
            }
            public string getConf()
            {
                return conf;
            }
            public void setText(string text)
            {
                this.text = text;
            }
            public string getText()
            {
                return text;
            }
            public void setWidth(string width)
            {
                this.width = width;
            }
            public string getWidth()
            {
                return width;
            }
        }
    }
}
